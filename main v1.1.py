############################################## Setup ################################################################
#####################################################################################################################
from machine import Pin
from time import sleep, time
import io
import _thread
ti0 = Pin(14, Pin.OUT, Pin.PULL_DOWN) #tool input 0 from the perspective of the ESP32. All IO is from this perspective
ti1 = Pin(13, Pin.OUT, Pin.PULL_DOWN) #tool input 1
to0 = Pin(25, Pin.IN, Pin.PULL_DOWN) #tool output 0
to1 = Pin(35, Pin.IN, Pin.PULL_DOWN) #tool output 1
gled = Pin(12, Pin.OUT, Pin.PULL_DOWN) #Green LED
rled = Pin(32, Pin.OUT, Pin.PULL_DOWN) #Red LED
btn = Pin(34, Pin.IN, Pin.PULL_DOWN) #Start test sequence button
seconds = 10 #here the test-time can be adjusted
boottime = 30 #boot timer
increment = 0 #a counter for the test function

############################################## Setup ################################################################

############################################## Boot ################################################################
#####################################################################################################################
def boot():
    global threadexitblnk #declaring the variable global so that it can be used by blink()
    threadexitblnk = False #A variable to check the status of this function is needed by the blink function so that the thread can be exited.
    print('Ready in ' + str(boottime) + ' seconds')
    timeout = time() + boottime #Setting the time the values are measured in the loop
    while time() != timeout: #this loop will run until the timer runs out
        pass #do nothing
    _thread.start_new_thread(blink, ()) #starting a thread for the blink function so that the program can blink and listen for the button at the same time
    while btn.value() != 1: #this loop will run until the button is pressed
        pass #do nothing
    threadexitblnk = True #set variable so that blink can exit.
        
def blink():
    global threadexitblnk #declaring that the varible declared in the boot function is to be used here.
    while not threadexitblnk: #while threadexitblnk is not True
        gled.value(1) 
        sleep(1)
        gled.value(0)
        sleep(1)
    gled.value(0)
    
############################################## Boot ################################################################

############################################## test function #############################################################
#####################################################################################################################
def test(tip0, tip1): #function to collect test and receives the test parameters tip0 and tip1 from the main loop
    global increment #declaring the increment variable global
    tests = '' #declaring a string to be used to contain the measured values from the test
    increment += 1 #adding 1 to the counter each time the test function is run
    print('Test ' + str(increment))
    ti0.value(tip0) #set the tool output to the value of the parameter tip0
    ti1.value(tip1) #set the tool output to the value of the parameter tip1
    timeout = time() + seconds #Setting the time the values are measured in the loop
    while not time() == timeout:    
        tests += str(to0.value()) + str(to1.value()) #append current value to the tests string
    f = open('test' + str(increment) + 'data.txt', 'r') #open the file with test data from previous tests to compare to. There are 4 files with test data, one for each test 1-4.
    if str(f.read())[10:20] in tests[10:20]: #compare a subset of the values measured with the testdata from the files
        f.close()
        return True
    else:
        f.close()
        return False

############################################## test function #############################################################

############################################## IO cleanup #############################################################
#####################################################################################################################
def cleanup(): #cleaning up the pins by setting them low.
    ti0.value(0)
    ti1.value(0)
    gled.value(0)
    rled.value(0)
    
############################################## IO cleanup #############################################################

############################################## Main ############################################################
#####################################################################################################################
try:
    while True:
        boot()
        if test(0,0) and test(0,1) and test(1,0) and test(1,1): #Each of the 4 tests are run in this single if statement
            print('Pass!')
            gled.value(1) #turn on the green LED to indicate successful test
        else:
            print('Fail!')
            rled.value(1) #turn on red LED to indicate failed test.
        while btn.value() != 1:
            pass
        cleanup()
        increment = 0
        while btn.value() != 1:
            pass
except KeyboardInterrupt:
    cleanup()
finally:
    cleanup()

    
############################################## Main loop ############################################################
