############################################## Setup ################################################################
#####################################################################################################################
from machine import Pin
from time import sleep, time
import io
ti0 = Pin(14, Pin.OUT, Pin.PULL_DOWN) #tool input 0 from the perspective of the ESP32. All IO is from this perspective
ti1 = Pin(13, Pin.OUT, Pin.PULL_DOWN) #tool input 1
to0 = Pin(25, Pin.IN, Pin.PULL_DOWN) #tool output 0
to1 = Pin(35, Pin.IN, Pin.PULL_DOWN) #tool output 1
gled = Pin(12, Pin.OUT, Pin.PULL_DOWN) #Green LED
rled = Pin(32, Pin.OUT, Pin.PULL_DOWN) #Red LED
seconds = 3 #here the test-time can be adjusted
increment = 0 #a counter for the test function


############################################## Setup ################################################################


############################################## test function #############################################################
#####################################################################################################################
def test(tip0, tip1): #function to collect test and receives the test parameters tip0 and tip1 from the main loop
    tests = '' #declaring a string to be used to contain the measured values from the test
    global increment #declaring the increment variable global
    increment += 1 #adding 1 to the counter each time the test function is run
    
    ti0.value(tip0) #set the tool output to the value of the parameter tip0
    ti1.value(tip1) #set the tool output to the value of the parameter tip1
    timeout = time() + seconds #Setting the time the values are measured in the loop
    while True:
        if time() == timeout: #terminate loop if time elapsed is equal to timeout
            f = open('test' + str(increment) + 'data.txt', 'r') #open the file with test data from previous tests to compare to. There are 4 files with test data, one for each test 1-4.
            if str(f.read())[10:20] in tests[10:20]: #compare a subset of the values measured with the testdata from the files
                f.close()
                cleanup()
                tests = '' #reset the tests string to be ready to receive new data.
                return True
            else:
                f.close()
                cleanup()
                tests = ''
                return False
        else:
            tests += str(to0.value()) + str(to1.value()) #append current value to the tests string
            print('to0-' + str(to0.value()) + 'to1-' + str(to1.value())) #Print the read values in terminal. for debugging purposes
        
############################################## test function #############################################################

############################################## IO cleanup #############################################################
#####################################################################################################################
def cleanup(): #cleaning up the pins by setting them low.
    ti0.value(0)
    ti1.value(0)
    
    
############################################## IO cleanup #############################################################

############################################## Main ############################################################
#####################################################################################################################
try:
    gled.value(0) #reset the LED pins to ensure they are set to default values.
    rled.value(0)
    while True:
        if test(0,0) and test(0,1) and test(1,0) and test(1,1): #Each of the 4 tests are run in this single if statement
            print('Pass!')
            gled.value(1) #turn on the green LED to indicate successful test
        else:
            print('Fail!')
            rled.value(1) #turn on red LED to indicate failed test.
        cleanup()
        break
except KeyboardInterrupt:
    cleanup()
    
############################################## Main loop ############################################################
